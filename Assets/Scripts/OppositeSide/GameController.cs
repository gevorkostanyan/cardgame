using System.Collections;
using UnityEngine;

namespace OppositeGame
{
    internal enum GameState
    {
        STARTED,
        PROGRESS,
        STOPPED,
    }

    public class GameController : MonoBehaviour
    {

        public static GameController Instance { get; private set; }

        [SerializeField]
        private GameObject Platform;

        [SerializeField]
        private CubeControll CubeLeft;
        [SerializeField]
        private CubeControll CubeRight;

        [SerializeField]
        private float speed = 4f;
        [SerializeField]
        private float bpm;

        public float deltaDistancePerSecond;

        private GameState gameState = GameState.STOPPED;

        private float timer = 0.0f;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            // Todo controll from UI. remove from here
            //gameState = GameState.STARTED;

            CubeLeft.UIColorChangeDelegate += UIManager.Instance.ChangeLeftButtonColor;
            CubeRight.UIColorChangeDelegate += UIManager.Instance.ChangeRightButtonColor;
        }

        void Update()
        {
            if (gameState == GameState.STARTED || gameState == GameState.PROGRESS)
            {
                timer += Time.deltaTime;
                int seconds = (int)(timer % 60);

                UIManager.Instance.ShowTime(seconds);
            }
        }

        private void FixedUpdate()
        {
            if (gameState == GameState.STARTED)
            {
                UIManager.Instance.StartAnimateButtons();
                StartCoroutine(MovePlatform());
                gameState = GameState.PROGRESS;
            }

            if (gameState == GameState.PROGRESS)
            {

            }
        }

        private IEnumerator MovePlatform()
        {
            float beatPerSecond = bpm / 60f;

            float startT = Time.fixedTime;

            Vector3 startPos = Platform.transform.position;
            deltaDistancePerSecond = beatPerSecond * speed;
            while (true)
            {
                Platform.transform.position = startPos + (-Vector3.forward * (Time.fixedTime - startT) * beatPerSecond * speed);
                yield return new WaitForFixedUpdate();
            }
        }

        // Todo define some logic for adding
        public void AddSpeed()
        {
            speed = speed + .5f;
        }

        public void StartGame()
        {
            gameState = GameState.STARTED;

            AudioManager.instance.StartBackgroundMusic();
        }

        public void StopGame()
        {
            gameState = GameState.STOPPED;
        }

        public void ChangeLeftColor()
        {
            CubeLeft.ChangeCubeMaterial();
        }

        public void ChangeRightColor()
        {
            CubeRight.ChangeCubeMaterial();
        }

        public GameObject GetPlatform()
        {
            return Platform;
        }
    }
}