using UnityEngine;

namespace OppositeGame
{
    public class PlatformObstacle : MonoBehaviour
    {
        [HideInInspector]
        private AudioSource AudioNoteSource;

        [HideInInspector]
        public Material material;

        private void Awake()
        {
            material = GetComponent<MeshRenderer>().materials[0];
            AudioNoteSource = GetComponent<AudioSource>();
        }

        public void PlaySound()
        {
            if (!AudioNoteSource.isPlaying)
            {
                AudioNoteSource.Play();
            }
        }
    }
}
