using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    enum SOUND
    {
        INTRO,
        MAIN,
        NONE
    }

    public static AudioManager instance;

    [SerializeField]
    private AudioSource audioMain;

    private SOUND soundState = SOUND.NONE;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    //void Start()
    //{
    //    StartBackgroundMusic();
    //}

    public void StartBackgroundMusic()
    {
        StartCoroutine(WaitForSound());
    }

    private IEnumerator WaitForSound()
    {
        yield return new WaitForSeconds(0);
        soundState = SOUND.MAIN;
    }

    private void Update()
    {
        audioMain.volume = .7f;

        if (!audioMain.isPlaying && soundState == SOUND.MAIN)
        {
            audioMain.Play();
            soundState = SOUND.NONE;
        }
    }
}
