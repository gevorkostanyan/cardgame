using UnityEngine;

namespace OppositeGame
{
    public class CubeControll : MonoBehaviour
    {
        private MeshRenderer meshRenderer;

        private Material currentMaterial;
        private Material obstacleMaterial;

        [SerializeField]
        private Material[] possibleMaterials;

        public delegate void OnColorChangedDelegate(Color color);
        public OnColorChangedDelegate UIColorChangeDelegate;

        private  new ParticleSystem particleSystem;

        private void Awake()
        {

            particleSystem = gameObject.transform.GetChild(0).GetComponent<ParticleSystem>();
            meshRenderer = GetComponent<MeshRenderer>();
            currentMaterial = meshRenderer.materials[0];

            if (possibleMaterials.Length == 0)
            {
                throw new System.Exception("Set possible materials first");
            }
        }

        public void ChangeCubeMaterial()
        {
            if (currentMaterial.name.Contains(possibleMaterials[0].name) || possibleMaterials[0].name.Contains(currentMaterial.name))
            {
                currentMaterial = possibleMaterials[1];
                UIColorChangeDelegate?.Invoke(possibleMaterials[0].color);
            }
            else
            {
                currentMaterial = possibleMaterials[0];
                UIColorChangeDelegate?.Invoke(possibleMaterials[1].color);
            }

            meshRenderer.material = currentMaterial;
        }


        public void SetRundomMaterial()
        {
            // Todo implement later like spetial skill
        }

        private void OnTriggerEnter(Collider other)
        {
            var platformObstacle = other.gameObject.GetComponent<PlatformObstacle>();
            obstacleMaterial = platformObstacle.material;

            if (currentMaterial.name.Contains(obstacleMaterial.name) || obstacleMaterial.name.Contains(currentMaterial.name))
            {
                currentMaterial = obstacleMaterial;
                meshRenderer.material = currentMaterial;

                platformObstacle.PlaySound();

                PlayParticle();

                // Todo delete with delay
                //Destroy(other.gameObject);
            }
            else
            {
                currentMaterial = obstacleMaterial;
                meshRenderer.material = currentMaterial;

                UIManager.Instance.ShakeCamera();
                // Todo play wrong sound
                // Play Particle effect
            }

            if (currentMaterial.name.Contains(possibleMaterials[0].name) || possibleMaterials[0].name.Contains(currentMaterial.name))
            {
                UIColorChangeDelegate?.Invoke(possibleMaterials[1].color);
            }
            else
            {
                UIColorChangeDelegate?.Invoke(possibleMaterials[0].color);
            }
        }

        private async void PlayParticle(){
            var main = particleSystem.main;
            main.startColor = currentMaterial.color;
            particleSystem.Play();
        }
    }
}