using System;
using UnityEngine;
using UnityEngine.UI;

namespace OppositeGame
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; private set; }

        [SerializeField]
        private UISpritesAnimation leftButtonSprites;

        [SerializeField]
        private UISpritesAnimation rightButtonSprites;

        private Button LeftButton;
        private Button RightButton;

        private Image LeftButtonImageColor;
        private Image RightButtonImageColor;

        [SerializeField]
        private Button play;

        [SerializeField]
        private GameObject gameControlls;
        [SerializeField]
        private GameObject UiMenu;

        [SerializeField]
        private Text TimerText;

        [SerializeField]
        private ShakeCamera cameraShaker;

        void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            ShowMainUI();

            RightButton = rightButtonSprites.gameObject.GetComponent<Button>();
            RightButton.onClick.AddListener(ChangeRightCubeColor);

            LeftButton = leftButtonSprites.gameObject.GetComponent<Button>();
            LeftButton.onClick.AddListener(ChangeLeftCubeColor);

            LeftButtonImageColor = leftButtonSprites.gameObject.GetComponent<Image>();
            RightButtonImageColor = rightButtonSprites.gameObject.GetComponent<Image>();

            play.onClick.AddListener(StartGame);

        }

        public void StartAnimateButtons()
        {
            leftButtonSprites.StartAnimation(null);
            rightButtonSprites.StartAnimation(null);
        }

        public void ChangeLeftCubeColor()
        {
            GameController.Instance.ChangeLeftColor();
        }

        public void ChangeRightCubeColor()
        {
            GameController.Instance.ChangeRightColor();
        }

        public void ChangeLeftButtonColor(Color color)
        {
            LeftButtonImageColor.color = color;
        }

        public void ChangeRightButtonColor(Color color)
        {
            RightButtonImageColor.color = color;
        }

        private void ShowMainUI()
        {
            gameControlls.SetActive(false);
            UiMenu.SetActive(true);
        }


        private void StartGame()
        {
            ShowGameControlls();
            GameController.Instance.StartGame();
        }

        private void ShowGameControlls()
        {
            gameControlls.SetActive(true);
            UiMenu.SetActive(false);
        }

        public void ShowTime(int seconds)
        {
            TimerText.text = seconds.ToString();
        }

        public void ShakeCamera()
        {
            cameraShaker.Shake();
        }
    }
}
