using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace OppositeGame
{
    [Serializable]
    public class Level1
    {
        public List<Obstacle> obstacles;
    }

    [Serializable]
    public class Obstacle
    {
        public Vector3 transform;
        public string obstacleName;
    }

    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField]
        private AudioSource YellowAudioSource;
        [SerializeField]
        private AudioSource RedAudioSource;
        [SerializeField]
        private AudioSource BlueAudioSource;
        [SerializeField]
        private AudioSource PinkAudioSource;

        [SerializeField]
        private GameObject yellowObstacle;
        [SerializeField]
        private GameObject redObstacle;
        [SerializeField]
        private GameObject blueObstacle;
        [SerializeField]
        private GameObject pinkObstacle;


        private Level1 level1;

        private void Start()
        {
            level1 = new Level1
            {
                obstacles = new List<Obstacle>()
            };

            Load();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameController.Instance.StartGame();
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                //Yellow
                CreateYellow();
                PlayYellowSound();
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                //Pink
                CreatePink();
                PlayPink();
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                //Blue
                CreateBlue();
                PlayBlue();
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                //Red
                CreateRed();
                PlayRed();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                Save(level1);
            }

            if(Input.GetKeyDown(KeyCode.LeftShift))
            {
                UIManager.Instance.ChangeLeftCubeColor();
            }

            if (Input.GetKeyDown(KeyCode.RightShift))
            {
                UIManager.Instance.ChangeRightCubeColor();
            }
        }

        private async void CreateYellow()
        {
            // X = -1.4, Y = 0.86
            var z = GameController.Instance.GetPlatform().transform.position.z;
            Vector3 yellowPosition = new Vector3(x: -1.4f, y: 0.86f, z: z);
            CreateAndAddObstacle(yellowPosition, "Yellow");
        }

        private async void PlayYellowSound()
        {
            YellowAudioSource.Play();
        }

        private async void CreatePink()
        {
            // X = -1.4, Y = 0.86
            var z = GameController.Instance.GetPlatform().transform.position.z;
            Vector3 pinkPosition = new Vector3(x: -1.4f, y: 0.86f, z: z);

            CreateAndAddObstacle(pinkPosition, "Pink");
        }

        private async void PlayPink()
        {
            PinkAudioSource.Play();
        }

        private async void CreateBlue()
        {
            // X = 1.4, Y = 0.86
            var z = GameController.Instance.GetPlatform().transform.position.z;
            Vector3 bluePosition = new Vector3(x: 1.4f, y: 0.86f, z: z);
            CreateAndAddObstacle(bluePosition, "Blue");
        }

        private async void PlayBlue()
        {
            BlueAudioSource.Play();
        }

        private async void CreateRed()
        {
            // X = 1.4, Y = 0.86
            var z = GameController.Instance.GetPlatform().transform.position.z;
            Vector3 redPosition = new Vector3(x: 1.4f, y: 0.86f, z: z);

            CreateAndAddObstacle(redPosition, "Red");
        }

        private async void PlayRed()
        {
            RedAudioSource.Play();
        }

        private void Load()
        {
            Debug.LogError("Path to level1.json = " + Application.dataPath + "/level1.json");

            if (File.Exists(Application.dataPath + "/level1.json"))
            {
                var loadedJson = File.ReadAllText(Application.dataPath + "/level1.json");
                var level = JsonUtility.FromJson<Level1>(loadedJson);

                foreach (Obstacle obstacle in level.obstacles)
                {
                    obstacle.transform.z *= -1;
                    switch (obstacle.obstacleName)
                    {
                        case "Yellow":
                            var yellowprefab = Instantiate(
                                yellowObstacle,
                                obstacle.transform,
                                Quaternion.identity
                            );
                            yellowprefab.transform.parent = GameController.Instance.GetPlatform().transform;
                            break;
                        case "Red":
                            var redPrefab = Instantiate(
                                redObstacle,
                                obstacle.transform,
                                Quaternion.identity
                            );
                            redPrefab.transform.parent = GameController.Instance.GetPlatform().transform;
                            break;
                        case "Blue":
                            var bluePrefab = Instantiate(blueObstacle,
                                                         obstacle.transform,
                                                         Quaternion.identity);
                            bluePrefab.transform.parent = GameController.Instance.GetPlatform().transform;
                            break;
                        case "Pink":
                            var pinkPrefab = Instantiate(pinkObstacle,
                                                         obstacle.transform,
                                                         Quaternion.identity);
                            pinkPrefab.transform.parent = GameController.Instance.GetPlatform().transform;
                            break;
                    }

                }
            }
        }

        private void Save(Level1 level)
        {
            string json = JsonUtility.ToJson(level);

            File.WriteAllText(Application.dataPath + "/level1.json", json);
            Debug.LogError(json);
        }

        private void CreateAndAddObstacle(Vector3 position, String obstacleName)
        {
            Obstacle obstacle = new Obstacle
            {
                transform = position,
                obstacleName = obstacleName
            };

            level1.obstacles.Add(obstacle);
        }
    }
}