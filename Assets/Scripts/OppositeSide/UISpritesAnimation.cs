using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace OppositeGame
{
    [RequireComponent(typeof(Image))]
    public class UISpritesAnimation : MonoBehaviour
    {
        public float duration;

        [SerializeField]
        private Sprite[] sprites;

        private Image image;
        private int index = 0;
        private float timer = 0;

        [SerializeField]
        private bool PlayOnstart = false;

        [SerializeField]
        private bool Repeatable = false;

        void Awake()
        {
            image = GetComponent<Image>();

            if (PlayOnstart)
            {
                if (Repeatable)
                {
                    StartCoroutine(StartAnimRepetable());
                }
                else
                {
                    StartCoroutine(StartAnim(null));
                }
            }
        }

        public void StartAnimation(Action action)
        {
            StopAllCoroutines();
            StartCoroutine(StartAnim(action));
        }

        public void StartAnimationRepeatable()
        {
            StopAllCoroutines();
            StartCoroutine(StartAnimRepetable());
        }

        public void StopAnimations()
        {
            StopAllCoroutines();
            image.sprite = sprites[sprites.Length - 1];
        }

        private IEnumerator StartAnim(Action action)
        {
            index = 0;

            while (true)
            {
                if (index == sprites.Length) break;

                if ((timer += Time.deltaTime) >= (duration / sprites.Length))
                {
                    timer = 0;
                    image.sprite = sprites[index];
                    index++;
                }

                yield return null;
            }

            yield return new WaitForEndOfFrame();

            action?.Invoke();
        }

        private IEnumerator StartAnimRepetable()
        {
            index = 0;

            while (true)
            {
                if (index == sprites.Length)
                {
                    index = 0;
                }

                if ((timer += Time.deltaTime) >= (duration / sprites.Length))
                {
                    timer = 0;
                    image.sprite = sprites[index];
                    index++;
                }


                yield return null;
            }
        }
    }
}