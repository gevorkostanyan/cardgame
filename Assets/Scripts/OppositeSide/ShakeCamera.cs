﻿using UnityEngine;

namespace OppositeGame
{
    public class ShakeCamera : MonoBehaviour
    {
        public Animator animator;

        public void Shake()
        {
            animator.enabled = true;

            animator.SetTrigger("shake");
        }
    }
}
