using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public Text winnerText;

    public GameObject panel;

    private void Awake()
    {
        MakeSingleton();
    }

    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        winnerText.enabled = false;
    }

    public void ShowWinner()
    {
        panel.SetActive(true);
        winnerText.enabled = true;
    }

    public void StartGame()
    {
        panel.SetActive(false);
        GameManager.instance.StartGame();
    }
}
