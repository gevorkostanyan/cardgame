using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public enum Participants
{
    Players2,
    Players4
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Animator animator;

    public Participants playerCount = Participants.Players2;

    public int currentGoalIndex;
    public Goal[] goals;

    public GameObject[] Deck;

    private GameObject[] player1 = new GameObject[4];
    private GameObject[] player2 = new GameObject[4];
    private GameObject[] player3 = new GameObject[4];
    private GameObject[] player4 = new GameObject[4];

    public GameObject[] participants;
    public SpriteRenderer[] playerIndicator;

    private GameObject currentFlippedCard;

    int currentPlayerIndex;

    void Awake()
    {
        MakeSingleton();
    }

    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void StartGame()
    {
        animator.Play("Scale");
        StartCoroutine(Shuffle(Deck));
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var hit = new RaycastHit();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                var key = hit.transform.parent.gameObject.name;
                Debug.Log("My object is clicked by mouse " + key);

                if (!IsPlayerHasCard(key))
                {
                    // this is not player hand
                    if (currentFlippedCard == null)
                    {
                        currentFlippedCard = hit.transform.parent.gameObject;
                        currentFlippedCard.GetComponent<Animator>().Play("FlipBackward");
                    }
                    else
                    {
                        // player do his step
                        currentFlippedCard.GetComponent<Animator>().Play("FlipForward");
                        currentFlippedCard = null;
                        ChangePlayer();
                    }
                }
                else
                {
                    if (currentFlippedCard != null)
                    {
                        // player do his step
                        Swap(currentFlippedCard, hit.transform.parent.gameObject);
                        hit.transform.parent.gameObject.GetComponent<Animator>().Play("FlipForward");
                        currentFlippedCard = null;
                    }
                }
            }
        }
    }

    private bool IsPlayerWin()
    {
        var goal = goals[currentGoalIndex];
        var cards = GetPLayerHand(currentPlayerIndex);

        if (cards[0] == null)
        {
            return false;
        }

        bool allSame = true;

        // Goal with any Suits
        if (goal.any)
        {
            Suit suit = cards[0].GetComponent<CardElement>().suit;
            for (int i = 1; i < cards.Length; i++)
            {
                if (cards[i] != null)
                {
                    if (suit != cards[i].GetComponent<CardElement>().suit)
                    {
                        allSame = false;
                        break;
                    }
                }
                else
                {
                    allSame = false;
                    break;
                }
            }

        }

        if (allSame && CheckHasCardCombination(cards))
        {
            // Here we has combo
            // Todo notify UI
        }

        return allSame;
    }

    private bool CheckHasCardCombination(GameObject[] cards)
    {
        bool hasCombo = true;
        var size = cards.Length;
        int[] tmp = new int[size];
        for (int i = 0; i < size; i++)
        {
            tmp[i] = cards[i].GetComponent<CardElement>().value;
        }

        Array.Sort(tmp);

        for (int i = 0; i < size - 1; i++)
        {
            if (tmp[i] != tmp[i + 1])
            {
                hasCombo = false;
                break;
            }
        }

        return hasCombo;
    }

    private GameObject[] GetPLayerHand(int playerIndex)
    {
        switch (playerIndex)
        {
            case 0:
                return player1;
            case 1:
                return player2;
            case 2:
                return player3;
            case 3:
                return player4;
        }

        throw new Exception("Wronr player index " + playerIndex);
    }

    private void ChangePlayer()
    {
        playerIndicator[currentPlayerIndex].enabled = false;
        if (playerCount == Participants.Players4)
        {
            if (currentPlayerIndex == 3)
            {
                currentPlayerIndex = -1;
            }
        }
        else
        {
            if (currentPlayerIndex == 1)
            {
                currentPlayerIndex = -1;
            }
        }
        playerIndicator[++currentPlayerIndex].enabled = true;
    }

    public void DetectPlayerCards(int playerIndex, int cardIndex, GameObject card)
    {
        GetPLayerHand(playerIndex)[cardIndex] = card;

        if (!IsPlayerWin())
        {
            ChangePlayer();
        }
        else
        {
            UIManager.instance.ShowWinner();
        }
    }

    private bool IsPlayerHasCard(string key)
    {
        int max = 4;
        if (currentPlayerIndex == 0)
        {
            for (int i = 0; i < max; i++)
            {
                if (player1[i].name == key)
                {
                    return true;
                }
            }
        }

        if (currentPlayerIndex == 1)
        {
            for (int i = 0; i < max; i++)
            {
                if (player2[i].name == key)
                {
                    return true;
                }
            }
        }

        if (currentPlayerIndex == 2)
        {
            for (int i = 0; i < max; i++)
            {
                if (player3[i].name == key)
                {
                    return true;
                }
            }
        }

        if (currentPlayerIndex == 3)
        {
            for (int i = 0; i < max; i++)
            {
                if (player4[i].name == key)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void DealRotateAnimationEnd()
    {
        playerIndicator[currentPlayerIndex].enabled = true;

        participants[0].SetActive(true);
        participants[1].SetActive(true);

        if (playerCount == Participants.Players4)
        {
            participants[2].SetActive(true);
            participants[3].SetActive(true);
        }

        Invoke("OpenCards", .5f);
    }

    private System.Random random = new System.Random();
    private IEnumerator Shuffle(GameObject[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            int iSwap1 = random.Next(i, list.Length - 1);
            int iSwap2 = random.Next(0, list.Length - 1);
            Swap(list[iSwap1], list[iSwap2]);
        }
        yield return new WaitForSeconds(1);

        animator.Play("Rotate");
    }

    private void Swap(GameObject gameObject1, GameObject gameObject2)
    {
        Transform tmpParent = gameObject1.transform.parent;
        Vector3 index1Pos = gameObject1.transform.position;
        Vector3 index1eluer = new Vector3(gameObject1.transform.eulerAngles.x, gameObject1.transform.eulerAngles.y, gameObject1.transform.eulerAngles.z);

        gameObject1.transform.position = gameObject2.transform.position;
        Vector3 eluer = new Vector3(gameObject2.transform.eulerAngles.x, gameObject2.transform.eulerAngles.y, gameObject2.transform.eulerAngles.z);
        gameObject1.transform.transform.rotation = Quaternion.Euler(eluer);
        gameObject1.transform.transform.SetParent(gameObject2.transform.parent.transform);

        gameObject2.transform.position = index1Pos;
        gameObject2.transform.rotation = Quaternion.Euler(index1eluer);
        gameObject2.transform.SetParent(tmpParent);
    }

    private void OpenCards()
    {
        var keys = 4;
        for (int i = 0; i < keys; i++)
        {
            player1[i].GetComponent<Animator>().Play("FlipBackward");
            player2[i].GetComponent<Animator>().Play("FlipBackward");
            if (playerCount == Participants.Players4)
            {
                player3[i].GetComponent<Animator>().Play("FlipBackward");
                player4[i].GetComponent<Animator>().Play("FlipBackward");
            }
        }
    }
}
