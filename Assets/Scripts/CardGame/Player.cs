using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum Card
{
    Card1,
    Card2,
    Card3,
    Card4
}

[System.Serializable]
public enum Seat
{
    Seat1,
    Seat2,
    Seat3,
    Seat4
}

public class Player : MonoBehaviour
{
    public Card card;
    public Seat seat;

    private void OnTriggerEnter(Collider other)
    {
        int playerIndex = 0;
        int cardIndex = 0;

        if (seat == Seat.Seat2)
        {
            playerIndex = 1;
        }

        if (seat == Seat.Seat3)
        {
            playerIndex = 2;
        }

        if (seat == Seat.Seat4)
        {
            playerIndex = 3;
        }

        if (card == Card.Card2)
        {
            cardIndex += 1;
        }

        if (card == Card.Card3)
        {
            cardIndex += 2;
        }

        if (card == Card.Card4)
        {
            cardIndex += 3;
        }

        GameManager.instance.DetectPlayerCards(playerIndex, cardIndex, other.gameObject.transform.parent.gameObject);
    }
}
