using System;
using UnityEngine;

[Serializable]
public enum Suit
{
    HEART,
    DIAMOND,
    SPADE,
    CLUB
}

public class CardElement : MonoBehaviour
{
    public Suit suit;

    [HideInInspector]
    private SpriteRenderer sprite;

    [Range(1, 13)]
    public int value;
}
