using UnityEngine;

public class NotifyAnimationEnd : MonoBehaviour
{
    void DealRotateAnimationEnd()
    {
        GameManager.instance.DealRotateAnimationEnd();
    }
}
